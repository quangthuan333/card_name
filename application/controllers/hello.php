<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');
  
class Hello extends CI_Controller {
  
    // Hàm khởi tạo
    function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->library("session");

    }
    public function index($message = '') {
        $message = 'abc';
        echo 'Freetuts.net Hello Controller ' .$message;
    }
    public function other(){
        echo 'Freetuts.net Other Controller';
    }
    public function index1(){
        $data=array(
            "username" => "Kaito",
            "email" => "codephp2013@gmail.com",
            "website" => "freetuts.net",
            "gender" => "Male",
        );
        $this->session->set_userdata($data);
        $this->session->userdata("username");
    }
}
