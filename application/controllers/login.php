<?php
class Login extends CI_Controller
{
    // Hàm load form login
    public function load_form()
    {
        // Load view
        $data = array(
            'title' => 'Đây là trang login',
            'message' => 'Nhập Thông Tin Đăng Nhập'
        );
        $this->load->view('login_view',$data);
        echo 'Freetuts.net Other Controller';
    }
    public function form()
    {
        // Load view lưu vào một biến
        $login_form = $this->load->view('login_view', '', true);
  
        // Xuất view ra màn hình
        echo $login_form;
    }
    public function __construct(){
        parent::__construct();
        // $this->load->library("database");
        $this->load->database();

    }
    public function getuser()
    {
        $query = $this->db->get("user");
        $data = $query->result_array();
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}