<?php
class User_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    public function listUser(){
        $this->db->select("id, name");
        $query=$this->db->get("user");
        return $query->result_array();
    }
}
?>